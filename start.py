#!/usr/bin/env python

import json
import os
from shutil import copyfile
import sys
from subprocess import Popen, PIPE

nginxSites = []

DOCKER_UP = "docker-compose -p {} up -d --build"
SCRIPT_PATH = os.path.abspath(os.path.dirname(sys.argv[0]))

COMPOSE_YML = SCRIPT_PATH + '/docker-compose.yml'

NGINX_PATH = SCRIPT_PATH + "/nginx/"
NGINX_CONF = NGINX_PATH + "/conf.d/"

DB_PORTS = {
    "redis": "6379",
    "mongo": "27017"
}

def printMessage(msg):
    msgLen = len(msg) + 10
    print "-" * msgLen
    print "|    " + msg + "    |"
    print "-" * msgLen + "\n"

def clone(src, dst):
    printMessage("Clonning repo: " + src)
    cloneStr = "git clone {} {}"

    if not os.path.isdir( dst ):
        retCode = os.system( cloneStr.format(src, dst) )
        if retCode:
            print "\nCloning Error"
            sys.exit(1)
        else:
            print "\nDONE!\n"
    else:
        print "Repo already exists!"

    print "\n"

def parseDockerson():
    printMessage("Parsing dockerson.json")
    data = None

    try:
        with open('dockerson.json') as dockerfile:
            data = json.load( dockerfile )
    except Exception, e:
        print "Dockerson File Error:", e
        sys.exit(1)

    repos = data['repos'] if "repos" in data else []
    dbs = data['dbs'] if "dbs" in data else []
    custom = data['custom'] if "custom" in data else []

    print "DONE!\n\n"
    return data['project'], repos, dbs, custom


def json2yaml(json, level=0):

    spaces = "  "
    new_line = "\n"
    to_print = ""
    for key, value in json.iteritems():

        to_print += (spaces*level) + key + ":"
        vType = type(value)

        if vType is dict:
            to_print += "\n" + json2yaml(value, level+1)
        elif vType is list:
            for item in value:
                to_print += new_line + (spaces*level+spaces) + "- " + item
            to_print += new_line
        else:
            to_print += " " + value + new_line

    return to_print


def writeLaravelService(project, repo, version):
    try:
        file = open(COMPOSE_YML, 'a')
        repoName = repo['name']
        repoPath = repo['path']

        dataToWrite = {
            repoName: {
                "build": "./php/7.0/" if version == 5 else "./php/5.6/",
                "working_dir": "/var/www/" + repoName,
                "volumes": [
                    repoPath + ":/var/www/" + repoName
                ],
                "networks": {
                    project: {
                        "aliases": [
                            repoName + ".app"
                        ]
                    }
                }
            }
        }
        if "hostname" in repo:
            dataToWrite[ repoName ][ "hostname" ] = repo["hostname"]

        file.write( os.linesep + os.linesep + json2yaml(dataToWrite, 1) )
    except Exception, e:
        print "Write Laravel Service Error ("+ project +"):", e
        sys.exit(1)
    finally:
        if file:
            file.close()


def writeRepoCompose(project, repo):
    rType = repo['type']

    if "laravel" in rType:
        if "4.x" in rType:
            writeLaravelService(project, repo, 4)
        else:
            writeLaravelService(project, repo, 5)

        if "domain" in repo:
            data = {
                "domain": repo['domain'],
                "name": repo['name']
            }
            nginxSites.append(data)


    elif "nodejs" in rType:
        writeNodeJSService(project, repo)


def startDCompose():
    with open(COMPOSE_YML, 'w') as file:
        file.write("version: '2'\n")
        file.write("services:\n")


def processPlugins(project, repo):
    printMessage('Running plugins for ' + repo['name'])
    if "plugins" in repo:
        plugins = repo['plugins']
        path = repo['path']
        name = repo['name']
        rType = repo['type']
        for plugin in plugins:
            if plugin == "laravel":
                laravelPlugin = './plugins/laravel.sh %s %s %s %s'
                version = rType.split("|")[1]
                os.system( laravelPlugin % (path, project, name, version) )
            elif plugin == "composer":
                composerPlugin = './plugins/composer.sh %s'
                os.system( composerPlugin % (path) )

    print '\nDONE!\n\n'

def createNginxConfs():
    printMessage('Creating nginx .conf files')
    if len(nginxSites) > 0:
        if not os.path.isdir(NGINX_CONF):
            os.mkdir(NGINX_CONF, 0755)

        for nginxSite in nginxSites:
            domain = nginxSite['domain']
            name = nginxSite['name']

            src = NGINX_PATH + "vhost.conf.template"
            dst = NGINX_CONF + name + ".conf"
            copyfile(src, dst)

            sed_vhost = "sed -i.bak 's/{{ %s }}/%s/g' %s"
            os.system( sed_vhost % ("repo", name, dst) )
            os.system( sed_vhost % ("domain", domain, dst) )

            os.system( "rm " + NGINX_CONF + "*.bak" ) 

    print "DONE!\n\n"

def writeNginxCompose(project):
    printMessage('Writing Nginx into docker-compose.yml')
    if len(nginxSites):
        try:
            volumes_link = []
            for nginxSite in nginxSites:
                volumes_link.append( nginxSite['name'] )
            dataToWrite = {
                "nginx-proxy": {
                    "image": "nginx:1.10",
                    "ports": [
                        "80:80"
                    ],
                    "networks": [
                        project
                    ],
                    "volumes_from": volumes_link,
                    "links": volumes_link,
                    "volumes": [
                        "./nginx/conf.d:/etc/nginx/conf.d"
                    ]
                }
            }
            with open(COMPOSE_YML, 'a') as file:
                    file.write( os.linesep + json2yaml(dataToWrite, 1) )
            print "DONE!\n\n"
        except Exception, e:
            print "Write Nginx Compose:", e
            sys.exit(1)


def writeNetworkCompose(project):
    printMessage('Writing networks into docker-compose.yml')
    try:
        dataToWrite = {
            "networks": {
                project: {
                    "driver": "bridge"
                }
            }
        }
        with open(COMPOSE_YML, 'a') as file:
            file.write( os.linesep + json2yaml(dataToWrite) )
        print "DONE!\n\n"
    except Exception, e:
        print "Write Network Compose:", e
        sys.exit(1)


def writeEtcHosts(project):
    printMessage('Overriding /etc/hosts')
    if len(nginxSites):
        sites = ""
        for nginxSite in nginxSites:
            sites += nginxSite['domain'] + " "

        sed = "sudo sed -i.bak '/%s/d' /etc/hosts > /dev/null"
        tee = "echo '%s' | sudo tee -a /etc/hosts > /dev/null"

        oldLine = ".*"+project+"-docker.*"
        newLine = "127.0.0.1 " + sites + "#" + project + "-docker"

        os.system( sed % (oldLine) )
        os.system( tee % (newLine) )
    print "DONE!\n\n"


def writeDBCompose(project, dbs):
    printMessage('Writing dbs into docker-compose.yml')
    try:
        if len(dbs):
            for db in dbs:
                dataToWrite = {
                    db: {
                        "build": "./" + db + "/",
                        "networks": {
                            project: {
                                "aliases": [
                                    db + ".db"
                                ]
                            }
                        },
                        "ports": [
                            DB_PORTS[db]+":"+DB_PORTS[db]
                        ]
                    }
                }
                with open(COMPOSE_YML, 'a') as file:
                    file.write( os.linesep + json2yaml(dataToWrite, 1) )

    except Exception, e:
        print "Write Network Compose Error:", e
        sys.exit(1)
    
    print "DONE!\n\n"


def writeCustoms(project, custom):
    printMessage('Writing custom into docker-compose.yml')
    for service in custom:
        try:
            with open(COMPOSE_YML, 'a') as file:
                file.write( os.linesep + json2yaml(service, 1) )

        except Exception, e:
            print "Write Custom Compose Error:", e
            sys.exit(1)

    print "DONE!\n\n"

def startContainers(project):
    printMessage('Starting containers')
    os.system( DOCKER_UP.format(project) )
    print "\nDONE!\n\n"

if __name__ == "__main__":
    project, repos, dbs, custom = parseDockerson()

    startDCompose()
    for repo in repos:
        proc = Popen(['echo ' + repo['into']], shell=True, stdout=PIPE)
        repo['path'] = proc.communicate()[0].strip()
        
        path = repo['path']
        clone( repo['clone'], path )
        
        writeRepoCompose(project, repo)

    createNginxConfs()

    writeNginxCompose(project)

    writeDBCompose(project, dbs)

    writeCustoms(project, custom)

    writeNetworkCompose(project)

    writeEtcHosts(project)

    startContainers(project)


    for repo in repos:
        processPlugins(project, repo)

